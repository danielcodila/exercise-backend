package com.exercise.rest.controller;

import com.exercise.rest.model.User;
import com.exercise.rest.service.UsersService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UsersController.class)
public class UsersControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UsersService usersService;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    public void readUsers() throws Exception {
        List<User> allUsers = prepareExpectedUsers();

        when(usersService.getUsers()).thenReturn(allUsers);

        mockMvc.perform(get("/api/users").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].firstName", is("firstName")))
                .andExpect(content().string(containsString("otherLastName")));
    }

    @Test
    public void createUser() throws Exception {
        User expectedFirstUser = prepareUser("first", "last", "mail@email.co");

        when(usersService.createUser(any(User.class))).thenReturn(expectedFirstUser);

        mockMvc.perform(post("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(expectedFirstUser)))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteUser() throws Exception {
        when(usersService.deleteUser(1)).thenReturn(ResponseEntity.noContent().build());

        mockMvc.perform(delete("/api/users/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    private List<User> prepareExpectedUsers() {
        User expectedFirstUser = prepareUser("firstName", "lastName", "email@email.com");
        User expectedSecondUser = prepareUser("otherFirstName", "otherLastName", "second@email.com");

        return Arrays.asList(expectedFirstUser, expectedSecondUser);
    }

    private User prepareUser(String first, String last, String s) {
        User expectedFirstUser = new User();
        expectedFirstUser.setFirstName(first);
        expectedFirstUser.setLastName(last);
        expectedFirstUser.setEmail(s);
        return expectedFirstUser;
    }

    // createUser_missingFirstName()
    // createUser_missingLastName()
    // createUser_missingEmail()
    // readOneUser()
    // readOneUser_exception()
    // updateUser()
    // updateUser_exception()
    // deleteUser_exception()

}