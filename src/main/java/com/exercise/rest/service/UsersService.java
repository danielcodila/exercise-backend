package com.exercise.rest.service;

import com.exercise.rest.exception.UserNotFoundException;
import com.exercise.rest.model.User;
import com.exercise.rest.repository.UsersRepository;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersService {
    private final UsersRepository usersRepository;

    public UsersService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @CacheEvict(value = "users", allEntries = true)
    public User createUser(User user) {
        return usersRepository.save(user);
    }

    @Cacheable("users")
    public List<User> getUsers() {
        return usersRepository.findAll();
    }

    public User getUserById(Integer id) {
        return usersRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
    }

    @CacheEvict(value = "users", allEntries = true)
    public User updateUser(Integer id, User userDetails) {
        User user = usersRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
        user.setFirstName(userDetails.getFirstName());
        user.setLastName(userDetails.getLastName());
        user.setEmail(userDetails.getEmail());

        return usersRepository.save(user);
    }

    @CacheEvict(value = "users", allEntries = true)
    public ResponseEntity<Void> deleteUser(Integer id) {
        try {
            usersRepository.deleteById(id);
            return ResponseEntity.noContent().build();
        } catch (EmptyResultDataAccessException e) {
            throw new UserNotFoundException(id);
        }
    }
}
