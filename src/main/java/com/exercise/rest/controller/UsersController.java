package com.exercise.rest.controller;

import com.exercise.rest.model.User;
import com.exercise.rest.service.UsersService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UsersController {

    private final UsersService usersService;

    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @PostMapping
    public User createUser(@Valid @RequestBody User user) {
        return usersService.createUser(user);
    }

    @GetMapping
    public List<User> readUsers() {
        return usersService.getUsers();
    }

    @GetMapping(value = "/{id}")
    public User readUser(@PathVariable("id") Integer id) {
        return usersService.getUserById(id);
    }

    @PutMapping("/{id}")
    public User updateUser(@PathVariable("id") Integer id, @Valid @RequestBody User user) {
        return usersService.updateUser(id, user);
    }


    @DeleteMapping(value = "/{id}")
    public void deleteUser(@PathVariable Integer id) {
        usersService.deleteUser(id);
    }

}
