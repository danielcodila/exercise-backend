### Docker
1) Maven clean install to compile, test and get the executable,
this step can be added to dockerfile as well. 

2) Build the image:

`docker build -t exercise/rest .`  

3) Run the container: 

`docker run -p 8080:8080 exercise/rest`   

The tag and ports can be adjusted.
